import requests
import matplotlib
# use backend to write to file instead of interactive
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime, timedelta
import pandas as pd
import random as rd
import configparser
#from PIL import Image

config = configparser.ConfigParser()
config.read("config.ini")

DEBUG_MODE = not config['debug'].getboolean('getValuesFromApi')

api_key = 'API_KEY'
athlete_id = config.get("intervals-auth", "athlete_id")
api_value = config.get("intervals-auth", "api_key")

getLastXDays = int(config.get("config", "getLastXDays"))

image_file = config.get("config", "intermediate-image")

# URL for the API request
base_url = 'https://intervals.icu/api/v1/athlete/' + athlete_id + '/'

def apiRequest(url):
    # Make the request with basic authentication
    response = requests.get(base_url + url, auth=(api_key, api_value))

    # Check if the request was successful
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Api returned Error: {response.status_code} for URL: {response.url}")
    return None

def getWellnessDataForDate(date):
    if DEBUG_MODE:
        return getDummyDataForDate(date)
    return apiRequest("wellness/" + date)

def getDummyDataForDate(date):
    ctl = rd.random()*50
    atl = (rd.random()-0.2)*40+ctl
    return {'id': date, 'ctl': ctl, 'atl': atl}

def printLoadLastDays(number_days):
    current_date = datetime.now().date()

    for i in range (number_days):
        date = current_date - timedelta(days=i)
        formatted_date = date.strftime("%Y-%m-%d")
        data = getWellnessDataForDate(formatted_date)
    
        print("date: " + str(data.get('id')))
        print("fitness: " + str(data.get('ctl')))
        print("fatigue: " + str(data.get('atl')))

def printChartLoadLastDays(number_days):
    current_date = datetime.now().date()

    dates = []
    fitness_values = []
    optimal_zone = []
    risk_zone = []
    grey_zone = []
    fatigue_values = []
    fresh_zone = []

    for i in range(number_days):
        # format time for api
        date = current_date - timedelta(days=i)
        formatted_date = date.strftime("%Y-%m-%d")
        data = getWellnessDataForDate(formatted_date)

        # fetch api data
        fitness_val = data.get('ctl')
        fatigue_values.append(data.get('atl'))
        dates.append(pd.to_datetime(formatted_date))

        # create custom data series
        optimal_zone.append(fitness_val+10)
        risk_zone.append(fitness_val+30)
        grey_zone.append(fitness_val-5)
        fresh_zone.append(fitness_val-20)
        fitness_values.append(fitness_val)

    # Reverse the lists to have chronological order
    dates.reverse()
    fitness_values.reverse()
    fatigue_values.reverse()
    risk_zone.reverse()
    optimal_zone.reverse()
    grey_zone.reverse()
    fresh_zone.reverse()


    # Plot the data
    plt.figure(figsize=(12, 5))

    # plot the data series
    #plt.plot(dates, fitness_values, label='Fitness')
    plt.plot(dates, fatigue_values, label='Fatigue', color=(1, 0, 0), linewidth=4)
    plt.plot(dates, optimal_zone, color='0', linestyle=(0,(5,5)), linewidth=2)
    plt.plot(dates, risk_zone, color='0', linestyle='-', linewidth=2)
    plt.plot(dates, grey_zone, color='0', linestyle=(0,(5,5)), linewidth=2)
    plt.plot(dates, fresh_zone, color='0', linestyle='-', linewidth=2)

    # specify fonts to use. The fonts need to be optized for good readability on low-res
    plt.rcParams["font.family"] = "sans-serif"
    fonts = ['DejaVu Sans', 'Verdana', 'Liberation Sans','Tahoma']
    plt.rcParams["font.sans-serif"] = fonts

    # Customize the x-axis
    ax = plt.gca()  # Get the current axis
    ax.xaxis.set_major_locator(mdates.DayLocator(interval=7))  # Set major ticks to every 7 days
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d.%m'))  # Format the date labels
    ax.xaxis.set_minor_locator(mdates.DayLocator(interval=1))  # Set minor ticks to every day

    # remove figure frame
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    # make ticks thick
    ax.xaxis.set_tick_params(width=5, which='both')
    ax.yaxis.set_tick_params(width=5)

    # Move ticks and labels inside the box of the figure, increase font size
    ax.tick_params(axis='x', which='both', direction='in', pad=-30, labelsize=25, labelfontfamily='sans-serif')
    ax.tick_params(axis='y', which='both', direction='in', pad=-40, labelsize=25, labelfontfamily='sans-serif')

    # display the x axis text horizontally instead of 45 degree
    plt.xticks(rotation=0)

    # set current weekday on the top left to identify not refreshing tags
    today = current_date.strftime("%a")
    title = ax.set_title(today, loc="left", fontsize=40, y=0.87)
    used_font = title.get_font_properties().get_name()
    if (fonts[0] != used_font):
        # warn if a selected font is not installed and cannot be used. If all specified fonts are not installed, the script will crash
        print("Font " + fonts[0] + " not found, using font " + used_font + " instead!")

    plt.tight_layout()
    plt.savefig(image_file, dpi=300)

#printLoadLastDays(getLastXDays)
#printChartLoadLastDays(getLastXDays)

#img = Image.open(image_file)
#img.show()