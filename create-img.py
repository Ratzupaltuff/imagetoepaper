import requests
from PIL import Image
import intervals_figure
import math
import statistics

import configparser

config = configparser.ConfigParser()
config.read("config.ini")

getLastXDays = int(config.get("config", "getLastXDays"))
mac = config.get("epaper", "mac")
apip = config.get("epaper", "ap-ip")

size = int(config.get("epaper", "screen-size-x")), int(config.get("epaper", "screen-size-y"))
display_colors = config.get("epaper", "color")
GENERATE_ONLY = config['epaper'].getboolean('generate-only')
DO_NOT_REFRESH_DATA = config['debug'].getboolean('doNotRefreshData')

image_file = config.get("config", "intermediate-image")
resized_file = "debug/resized_file.png"
output_file = config.get("config", "output-image")

dither = 0   # set dither to 1 is you're sending photos etc, you'll get compression artefacts as a tradeoff

# Define the color palette (white, black, and optionally red)
palette = [
    255, 255, 255,  # white
    0, 0, 0        # black
]
if (display_colors == "bwr"):
    palette = [
        255, 255, 255,  # white
        0, 0, 0,        # black
        255, 0, 0       # red
    ]

# Create a new paletted image with indexed colors
proper_image = Image.new('P', (size[0], size[1]))
# Assign the color palette to the image
proper_image.putpalette(palette)

def resize_canvas(old_image_path, new_image_path,
                  canvas_width=size[0], canvas_height=size[1]):
    """
    Place one image on another image.

    Resize the canvas of old_image_path and store the new image in
    new_image_path. Center the image on the new canvas.
    """
    im = Image.open(old_image_path)
    im.convert(palette=proper_image)
    im.thumbnail(size, Image.Resampling.HAMMING)

    old_width, old_height = im.size

    mode = im.mode
    if len(mode) == 1:  # L, 1
        new_background = (255)
    if len(mode) == 3:  # RGB
        new_background = (255, 255, 255)
    if len(mode) == 4:  # RGBA, CMYK
        new_background = (255, 255, 255, 255)

    newImage = Image.new(mode, (canvas_width, canvas_height), new_background)

    # center resized image in canvas
    x1 = int(math.floor((canvas_width - old_width) / 2))
    y1 = int(math.floor((canvas_height - old_height) / 2))
    newImage.paste(im, (x1, y1, x1 + old_width, y1 + old_height))

    # save as png (jpeg would create compression artefacts)
    newImage.save(new_image_path)

# Function to calculate Euclidean distance between two colors
def color_distance(c1, c2):
    return math.sqrt(sum((a - b) ** 2 for a, b in zip(c1, c2)))

def get_color_in_palette(palette, number):
    return (palette[number], palette[number+1], palette[number+2])

def get_nearest_color(color_map, pixel):
    # check if pixel is white
    default_color = get_color_in_palette(color_map, 0)
    if(color_distance(pixel[0:2], default_color)<40): 
        #adjust threshhold to 20 to have more black pixel around text
        return 0
    
    #check if pixel is a grey value
    variance_in_pixel = statistics.variance(pixel[0:2])
    if (variance_in_pixel<10):
        return 1 #return black
    return 2 #return red

# Map colors to the palette by finding the closest color
def map_colors(image):
    indexed_data = []
    for pixel in image.getdata():
        closest_color = get_nearest_color(palette, pixel)
        indexed_data.append(closest_color)
    return indexed_data

resize_canvas(image_file, resized_file)
figure = Image.open(resized_file)
indexed_figure = None

if (display_colors == "bwr"):
    # with bwr the palletization is more difficult
    indexed_figure = Image.new('P', figure.size)
    indexed_data = map_colors(figure)
    indexed_figure.putdata(indexed_data)
    indexed_figure.putpalette(palette)
    # Save the image as JPEG with maximum quality
    indexed_figure.convert("RGB").save(output_file, 'JPEG', quality="maximum")
else:
    proper_image.paste(figure)
    # Save the image as JPEG with maximum quality
    proper_image.convert("RGB").save(output_file, 'JPEG', quality="maximum")


def sendImage():
    # Prepare the HTTP POST request
    url = "http://" + apip + "/imgupload"
    payload = {"dither": dither, "mac": mac}  # Additional POST parameter
    files = {"file": open(output_file, "rb")}  # File to be uploaded

    # Send the HTTP POST request
    response = requests.post(url, data=payload, files=files)

    # Check the response status
    if response.status_code == 200:
        print("Image uploaded successfully!")
    else:
        print("Failed to upload the image.")

if (not DO_NOT_REFRESH_DATA):
    intervals_figure.printChartLoadLastDays(getLastXDays)

img = Image.open(output_file)
#img.show() # display output image with image viewer of the operating system
if (not GENERATE_ONLY):
    sendImage()